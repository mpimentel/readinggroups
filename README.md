# readinggroups

Slides for reading group seminar. Code based heavily on different libraries (check the requirements for each notebook).

Notebook can be viewed directly in gitlab by clicking on [Personalized_Risk_Scoring_Mixtures_Gaussian_Processes.ipynb](https://gitlab.com/mpimentel/readinggroups/blob/master/Personalized_Risk_Scoring_Mixtures_Gaussian_Processes.ipynb) 

## How to run these slides and examples yourself

**Setup python environment**

- Jupyter notebook
- Requirements: `numpy`, `gpflow`, `jupyter`, `seaborn`
- Python 3
- Install RISE for an interactive presentation viewer 
  - see install instructions here (https://github.com/damianavila/RISE) 

**Get**
```bash
# Clone the repository
git clone https://gitlab.com/mpimentel/readinggroups
cd readinggroups
```

**Run**
```
jupyter notebook
```

**View in presentation mode using RISE**   
After running the notebook, click on the RISE button
